import { Col, Row, Jumbotron, Button, Card } from 'react-bootstrap';
import Link from 'next/link';
import PropTypes from 'prop-types'

export default function Banner({ dataProp }) {
	const {title, content, destination, label} = dataProp;

	return (
		<Row>
			<Col>
				<Card className="cardHighlight">
					<Card.Body>
						<h1>{title}</h1>
						<p>{content}</p>
						<Link href={destination}>
							<a role="button">{label}</a>
						</Link>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}

Banner.propTypes = {
	data: PropTypes.shape({
		title: PropTypes.string.isRequired,
		content: PropTypes.string.isRequired,
		destination: PropTypes.string.isRequired,
		label: PropTypes.string.isRequired
	})
}