import { Row, Col, Card } from 'react-bootstrap'

export default function Highlights() {
	return (
		<Row>
			<Col>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<Card.Img variant="top" src="https://i.pinimg.com/originals/a6/05/53/a6055300e27313cb3ad1a886e9dd74ca.png" />
						</Card.Title>
						<Card.Text className="text-center">
							Realtime Income/Expense Reports
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
			<Col>
				<Card className="cardHighlight">
					<Card.Body>
						<Card.Title>
							<Card.Img variant="top" src="https://i.pinimg.com/originals/a6/05/53/a6055300e27313cb3ad1a886e9dd74ca.png" />
						</Card.Title>
						<Card.Text className="text-center">
							Realtime Pie & Line Charts
						</Card.Text>
					</Card.Body>
				</Card>
			</Col>
		</Row>
	)
}