import { useState, useEffect } from 'react';
import { Table, Card, Button } from 'react-bootstrap';
import PropTypes from 'prop-types';

export default function TransactionCard({ transactionProp }) {
    
    const {name, description, price, transactionType} = transactionProp;

    const [count, setCount] = useState(0);
    const [seats, setSeats] = useState(100);
    const [isOpen, setIsOpen] = useState(true);

    function enroll(transactionId){
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/enroll`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                transactionId: transactionId
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            console.log(data)
        })
    }

    useEffect(() => {
        if(seats === 0){
            setIsOpen(false);
        }
    }, [seats]);

    return (
        <tr key={transactionType}>
            <td>{name}</td>
            <td>{description}</td>
            <td>{price}</td>
            <td>{transactionType}</td>
        </tr>        
    )
}

TransactionCard.propTypes = {
    // shape() - used to check that the prop conforms 
    // to a specific "shape"
    transactionProp: PropTypes.shape({
        name: PropTypes.string.isRequired,
        description: PropTypes.string.isRequired,
        price: PropTypes.number.isRequired,
        transactionType: PropTypes.string.isRequired
    })
}