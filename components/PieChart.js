import { useState, useEffect } from 'react'
import { Pie } from 'react-chartjs-2'
import { colorRandomizer } from '../helpers/helpers'

export default function PieChart({ rawData }){

    const [types, setTypes] = useState([])
    const [price, setPrice] = useState([])
    const [bgColors, setBgColors] = useState([])

    // state for storing JWT
	const [token, setToken] = useState('');

	// sets Token from localstorage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	})

    useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
        .then(res => res.json())
        .then(data => {
        let names = []

        data.forEach(element => {
            if(!names.find(name => name === element.name)){
             names.push(element.name)
            }
        })
        console.log(names)
        setTypes(names)
        })    
    }, [rawData])

 
    useEffect(() => {

         fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
        })
         .then(res => res.json())
         .then(data => {
            setPrice(types.map(type => {

                let price = 0

                data.forEach(element => {
                    if(element.name === type){
                        price = price + parseInt(element.price)
                    }
                })
                return price
            	
            	console.log(price)
            }))
         })

		setBgColors(types.map(() => `#${colorRandomizer()}`))
    }, [types])
   
   
    const data = {
        labels: types,
        datasets: [{
            data: price,
            backgroundColor: bgColors,
            hoverBackgroundColor: bgColors
        }]
    }

    return (
        <Pie data={data} />
    )
}

