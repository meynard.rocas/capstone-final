import { useState, useEffect } from 'react'
import { Line } from 'react-chartjs-2'
import moment from 'moment'

export default function LineChart({ rawData }){
    const [months, setMonths] = useState([])
    const [monthly, setMonthly] = useState([1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12])
    const [types, setTypes] = useState([])
    const [price, setPrice] = useState([])
    const [income, setIncome] = useState([])
    const [expenses, setExpenses] = useState([])
    const [lineData, setLineData] = useState({})

    // state for storing JWT
    const [token, setToken] = useState('');

    // sets Token from localstorage
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    })

      useEffect(() => {
            fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
                headers: {
                    'Authorization': `Bearer ${localStorage.getItem('token')}`
                }
            })
            .then(res => res.json())
            .then(data => {
            let transactionTypes = []
                data.forEach(element => {
                    if(!transactionTypes.find(transactionType => transactionType === element.transactionType)){
                     transactionTypes.push(element.transactionType)
                    }
                })
                setTypes(transactionTypes)
            })    
    }, [rawData])
       useEffect(() => {
         fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`,{
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
         })
         .then(res => res.json())
         .then(data => {
            console.log(111, data)
             setPrice(types.map(type => {
                let price = 0
                data.forEach(element => {
                   if(element.transactionType === type){
                        price = price + parseInt(element.price)
                    }
                })
                return price
            }))
         })
    }, [types])
       console.log(price)
        console.log(111, income)
         console.log(111, expenses)




     useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
            })
            .then(res => res.json())
            .then(data => {
                 if(data.length > 0){
                    let tempDaysIncome = [ 0, 0, 0, 0, 0, 0, 0]
                    let tempDaysExpense = [ 0, 0, 0, 0, 0, 0, 0]
                    data.forEach(element => {
                        if (element.transactionType == "Income") {
                            tempDaysIncome[moment(element.createdOn).day()]
                            += element.price
                        }
                        else {
                            tempDaysExpense[moment(element.createdOn).day()]
                            += Math.abs(element.price)
                        }
                    })
                    setLineData({
                        labels: ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"],
                        datasets: [
                            {
                                label: "Daily Income",
                                data: tempDaysIncome,
                                borderColor: ["#005678"],
                                borderWidth: 2
                            },
                            {
                                label: "Daily Expense",
                                data: tempDaysExpense,
                                borderColor: ["#6E3094"],
                                borderWidth: 2
                            }

                        ]
                    })

                    let tempMonths = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
                    data.forEach(element => {
                        console.log(moment(element.createdOn).format('MMMM'))
                        if(!tempMonths.find(month => month === moment(element.createdOn).format('MMMM'))){
                    tempMonths.push(moment(element.createdOn).format('MMMM'))
                   
                }
            })          
            setMonths(tempMonths)
            }
        }
        )

    }, [rawData])
     useEffect(() => {
        fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
            headers: {
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            }
            })
        .then(res => res.json())
        .then(data => {
             setMonthly(months.map(month => {
            let price = 0
            let income = 0
            let expenses = 0

            data.forEach(element => {
                console.log(moment(element.createdOn).format('MMMM'))
                if(moment(element.createdOn).format('MMMM') === month){
                    price = price + parseInt(element.price)
                if(element.transactionType === 'Income'){
                    income  += element.price
                }
                 if(element.transactionType === 'Expense'){
                    expenses  += Math.abs(element.price)//+amount
                }
                }      
            })
            setIncome(prev => [...prev , income])
            setExpenses(prev => [...prev , expenses])
            return price
            }))
        })
    }, [months])  
        console.log(monthly)
        const data = {
                    labels: months,
                    datasets: [{
                        label: types[0],
                        data: income,
                        borderColor: ["#005678"],
                        borderWidth: 2
                    },
                    {
                        label: types[1],
                        data: expenses,
                        borderColor: ["#6E3094"],
                        borderWidth: 2
                    },
                    {
                        label: 'Total Balance',
                        data: monthly,
                        borderColor: ["#F2A490"],
                        borderWidth: 2
                    }
                    ]
                }  
    console.log(data)
    const options = {
        title: {
            display: true,
            text: 'Monthly Record'
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        min: 0
                    }
                }
            ]
        }
    }

        const optionsWeekly = {
        title: {
            display: true,
            text: 'Daily Record'
        },
        scales: {
            yAxes: [
                {
                    ticks: {
                        min: 0
                    }
                }
            ]
        }
    }
    return(
        <div>
        <br/>
            <Line data={ data } options = { options }/>
            <br />
            <Line data={ lineData } options = { optionsWeekly }/>
            <br />
            <br />
        </div>
    )
}