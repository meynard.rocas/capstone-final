import { useContext } from 'react';
import { Container, Nav, Navbar, NavDropdown } from 'react-bootstrap'
import UserContext from '../UserContext';
import Link from 'next/link';

export default function NavBar() {
    
    const { user } = useContext(UserContext);

    return (

        <Container expand="lg">
        
            <Navbar bg="info" variant="dark" expand="lg">
                <Navbar.Brand href="">PiggyBank</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />

                <Navbar.Collapse id="responsive-navbar-nav">

                    <Nav className="mr-auto">
                        {(user.email !== null)
                            ?
                            <React.Fragment>
                                <Link href="/transactions">
                                    <a className="nav-link" role="button">INPUT</a>
                                </Link>
                                <Link href="/transactions/history">
                                    <a className="nav-link" role="button">HISTORY</a>
                                </Link>
                                <Link href="/transactions/chart">
                                    <a className="nav-link" role="button">CHART</a>
                                </Link>
                                <Link href="/logout">
                                    <a className="nav-link" role="button">SIGN OUT</a>
                                </Link>
                            </React.Fragment>
                            : 
                            <React.Fragment>

                                <Link href="/">
                                    <a className="nav-link" role="button">SIGN IN</a>
                                </Link>
                                <Link href="/register">
                                    <a className="nav-link" role="button">CREATE AN ACCOUNT</a>
                                </Link>

                            </React.Fragment>
                        }
                    </Nav>
                    
                </Navbar.Collapse>

            </Navbar>

        </Container>            
        
    )
}