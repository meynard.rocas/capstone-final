import { useContext, useState, useEffect } from 'react';
import { Table, Modal, Form, Button, Row, Col, Card, Container, CardColumns, CardGroup, CardDeck } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head';
import Router from 'next/router';
import View from '../../components/View';

export default function index({ data }) {

    // states for edit form input
    const [transactionName, setTransactionName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [transactionType, setTransactionType] = useState('');


    // state for storing JWT
    const [token, setToken] = useState('');

    // sets Token from localstorage
    useEffect(() => {
        setToken(localStorage.getItem('token'))
    })

    //function for processing creation of a new transaction
    function addTransaction(e) {

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${localStorage.getItem('token')}`
            },
            body: JSON.stringify({
                name: transactionName,
                description: description,
                price: price,
                transactionType: transactionType
            })
        })
        .then(res => {
            return res.json()
        })
        .then(data => {
            if (data === true){
                Router.push('/transactions')

            } else {
                Router.push('/errors/1')

            }
        })
    }

    return (

        <React.Fragment>

            <Container expand="lg">

                <Card className="text-center shadow-lg mt-5">

                    <Form onSubmit={(e) => addTransaction(e)} className="ml-5 mr-5">

                        <Form.Group controlId="transactionType">

                          <Form.Label className="mt-5 text-primary">
                            POST A NEW TRANSACTION
                          </Form.Label>
                          
                          <Form.Control
                            as="select"
                            onClick={e => setTransactionType(e.target.value)}
                            required
                            className="shadow text-muted"
                          >
                            <option value="Income">Income</option>
                            <option value="Expense">Expense</option>
                          </Form.Control>
                        </Form.Group>

                        <Form.Group controlId="transactionName" className="shadow">

                            <Form.Control 
                                type="text"
                                placeholder="Transaction Name"
                                value={transactionName}
                                onChange={e => setTransactionName(e.target.value)}
                                required
                                
                            />
                        </Form.Group>

                        <Form.Group controlId="description" className="shadow">

                            <Form.Control
                                as="textarea"
                                rows="3"
                                placeholder="Description"
                                value={description}
                                onChange={e => setDescription(e.target.value)}
                                required
                                
                            />
                        </Form.Group>

                        <Form.Group controlId="price" className="d-flex flex-row shadow mb-5">
                            <Form.Control
                                type="text"
                                placeholder="Price"
                                value={price}
                                onChange={e => setPrice(e.target.value)}
                                required
                                
                            />
                            <Button variant="outline-primary" type="submit">
                                +ADD 
                            </Button>
                        </Form.Group>
                        
                    </Form>
                </Card>

            </Container>

        </React.Fragment>
    
    )
}

export async function getServerSideProps() {
    // fetch data from endpoint
    const res = await fetch (`${process.env.NEXT_PUBLIC_API_URL}/transactions`)
    const data = await res.json()

    //return the props
    return {
        props: {
            data
        }
    }
}