import { useContext, useState, useEffect } from 'react';
import { Form, Button, Row, Col, Alert, Card, Container } from 'react-bootstrap'

import Head from 'next/head'
import PieChart from '../../components/PieChart'
import LineChart from '../../components/LineChart';
import UserContext from '../../UserContext';
import View from '../../components/View';
import moment from 'moment'


export default function chart({data}){
    console.log(data)
	
	const month = moment().format('MMMM');
    const year = moment().format('YYYY');
 
	return (
		<React.Fragment>

            <Head>
                <title>Transaction Analytics</title>
            </Head>

            <Container expand="lg">

                <Card className="text-center shadow-lg mt-5 mb-5">

                    <br/>
            		<h3 className="text-center chartHeading m-3" style={{color: "#414167"}}>
            	          Budget Summary for {month} {year}
            	    </h3>

                    <div className="m-3">

                        <PieChart rawData={data} /> 

                        <LineChart rawData={data} /> 

                    </div>

                </Card>

            </Container>

        </React.Fragment>
	)
}


export async function getServerSideProps() {
	// fetch data from endpoint
	const res = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`)
	const data = await res.json()

	//return the props
	return {
		props: {
			data
		}
	}
}