import { useContext, useState, useEffect } from 'react';
import TransactionCard from '../../components/TransactionCard';
import { Table, Modal, Form, Button, Row, Col, Card, Container, CardColumns, CardGroup, CardDeck } from 'react-bootstrap';
import UserContext from '../../UserContext';
import Head from 'next/head';
import Router from 'next/router';

export default function history({ data }) {

	const { user } = useContext(UserContext);

	const [targetTransactionName, setTargetTransactionName] = useState('')

	// states for edit form input
	const [transactionName, setTransactionName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState('');
	const [transactionType, setTransactionType] = useState('');

	// state for transaction ID to be edited
	const [transactionId, setTransactionId] = useState('');

	// state for showing and closing the modal edit form
	const [showForm, setShowForm] = useState(false);

	// state for storing JWT
	const [token, setToken] = useState('');

	// sets Token from localstorage
	useEffect(() => {
		setToken(localStorage.getItem('token'))
	})

	const handleCloseUpdate = () => setShowForm(false);

	const handleShowUpdate = (transactionId) => {

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/${transactionId}`)
		.then(res => {
			return res.json()
		})
		.then(data => {
			setTransactionId(data._id)
			setTransactionName(data.name)
			setDescription(data.description)
			setPrice(data.price)
			setTransactionType(data.transactionType)
			setShowForm(true);
		})

	}

	function editTransaction(e) {
		e.preventDefault();

		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				'Authorization': `Bearer ${token}`
			},
			body: JSON.stringify({
				transactionId: transactionId,
				name: transactionName,
				description: description,
				transactionType: transactionType,
				price: price
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {

			// if update transaction successful
			if(data === true){
				Router.push('/transactions/history')
				setShowForm(false)
			}else{
				// error in editing page
				Router.push('/errors/1')
				setShowForm(false)
			}

		})
	}

	function deleteTransaction(transactionId) {
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/transactions/${transactionId}`, {
			method: 'DELETE',
			headers: {
				'Authorization': `Bearer ${token}`
			}
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			// if archive transaction succesful
			if(data === true){
				Router.push('/transactions/history')
				setShowForm(false)

			} else {
				// error in archiving transaction
				Router.push('/errors/1')
				setShowForm(false)
			}
		})
	}

	const transactionRows = data.map(indivTransaction => {

		return (

			<tr key={indivTransaction._id}>
				<td>{indivTransaction.name}</td>
				<td>{indivTransaction.description}</td>
				<td>{indivTransaction.price}</td>
				<td>{indivTransaction.transactionType}</td>
				<td>
					<Button className="bg-primary" onClick={() => {handleShowUpdate(indivTransaction._id)}}>Edit</Button>
					<Button className="bg-danger" onClick={() => {deleteTransaction(indivTransaction._id)}}>Delete</Button>
				</td>
			</tr>
		)
	
	})
	
	const transactionFilter = data.map(indivTransaction => {

		if(indivTransaction.transactionType === transactionType){

			return (
				<tr key={indivTransaction._id}>
					<td>{indivTransaction.name}</td>
					<td>{indivTransaction.description}</td>
					<td>{indivTransaction.price}</td>
					<td>{indivTransaction.transactionType}</td>
					<td>
						<Button className="bg-primary" onClick={() => {handleShowUpdate(indivTransaction._id)}}>Edit</Button>
						<Button className="bg-danger" onClick={() => {deleteTransaction(indivTransaction._id)}}>Delete</Button>
					</td>
				</tr>
			)
		} 
		
	})

	const transactionSearch = data.map(indivTransaction => {

		if(indivTransaction.name.toLowerCase() === transactionName.toLowerCase()){

			return(
				<tr key={indivTransaction.name}>
	                <td>{indivTransaction.name}</td>
	                <td>{indivTransaction.description}</td>
	                <td>{indivTransaction.price}</td>
	                <td>{indivTransaction.transactionType}</td>
	                <td>
						<Button className="bg-primary" onClick={() => {handleShowUpdate(indivTransaction._id)}}>Edit</Button>
						<Button className="bg-danger" onClick={() => {deleteTransaction(indivTransaction._id)}}>Delete</Button>
					</td>
	            </tr>
			)
		} 

	})

	return (

	<React.Fragment>

		<Head>
			<title>Transaction History</title>
		</Head>

		<Modal show={showForm} onHide={handleCloseUpdate}>

			<Modal.Header closeButton>
				<Modal.Title></Modal.Title>
			</Modal.Header>

			<Modal.Body>

				<Form onSubmit={(e) => editTransaction(e)}>

					<Form.Group controlId="transactionName">
						<Form.Label>Transaction Name:</Form.Label>
						<Form.Control 
						type="text" 
						placeholder="Enter transaction name" 
						value={transactionName} 
						onChange={e => {setTransactionName(e.target.value)}} 
						required
						/>
					</Form.Group>

					<Form.Group controlId="description">
						<Form.Label>Transaction Description:</Form.Label>
						<Form.Control 
						as="textarea" 
						rows="3" 
						placeholder="Enter transaction description" 
						value={description} 
						onChange={e => {setDescription(e.target.value)}} 
						required
						/>
					</Form.Group>

					<Form.Group controlId="price">
						<Form.Label>Transaction Price:</Form.Label>
						<Form.Control 
						type="number" 
						value={price} 
						onChange={e => {setPrice(e.target.value)}} 
						required
						/>
					</Form.Group>

					<Form.Group controlId="transactionType">
						<Form.Label>Transaction Type:</Form.Label>
						<Form.Control 
						type="text" 
						placeholder="Enter transaction type" 
						value={transactionType} 
						onChange={e => {setTransactionType(e.target.value)}} 
						required
						/>
					</Form.Group>

					<Button className="bg-primary" type="submit">Submit</Button>
					</Form>
				</Modal.Body>

				<Modal.Footer>
					<Button className="bg-secondary" onClick={handleCloseUpdate}>
					Close
					</Button>
			</Modal.Footer>

		</Modal>

		<Container expand="lg">
		
	   		<h3 className="text-center m-4">Your Transaction Records</h3>

	        	<Card className="text-center shadow-lg mt-5">

					<Table>
						<thead>
							<tr>
								<td>Name</td>
								<td>Description</td>
								<td>Amount (PHP)</td>
								<td>Type</td>
								<td>Edit/Delete</td>
							</tr>
						</thead>
						<tbody>
							{ transactionRows }
						</tbody>
					</Table>
					
				</Card>

				<h3 className="text-center m-4">Your Transaction Records</h3>

				<Form>
					<Form.Group className="d-flex flex-row mt-5">

					    <Form.Control 
                            type="text"
                            placeholder="Search"
                            value={transactionName}
                            onChange={e => setTransactionName(e.target.value)}
                            className="shadow text-muted"
                        />

						<Form.Control
							as="select"
							onChange={e => setTransactionType(e.target.value)}
							className="shadow text-muted"
							
						>
							<option value="">None</option>
							<option value="Income">Income</option>
							<option value="Expense">Expense</option>
						</Form.Control>
					</Form.Group>

				</Form>

				<Card className="text-center shadow-lg mt-5">

					<Table>
						<thead>
							<tr>
								<td>Name</td>
								<td>Description</td>
								<td>Amount (PHP)</td>
								<td>Type</td>
								<td>Edit/Delete</td>
							</tr>
						</thead>
						<tbody>
						
							{ transactionFilter }
							{ transactionSearch }
	                        
	                    </tbody>
					</Table>

				</Card>


		</Container>

	</React.Fragment>
	
	)
}

export async function getServerSideProps() {
	// fetch data from endpoint
	const res = await fetch (`${process.env.NEXT_PUBLIC_API_URL}/transactions`)
	const data = await res.json()

	//return the props
	return {
		props: {
			data
		}
	}
}

