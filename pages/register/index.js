import { useState, useEffect } from 'react';
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap';
import Router from 'next/router';
import View from '../../components/View';

export default function index() {


	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [isActive, setIsActive] = useState(false);

	function registerUser(e) {
		e.preventDefault();

		// used to send requests
		// fetch('url', {options})

		// check for duplicate email
		fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/email-exists`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => {
			return res.json()
		})
		.then(data => {
			//if no duplicates found -> register
			if (data === false){
				fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`, {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						firstName: firstName,
						lastName: lastName,
						email: email,
						password: password1,
						mobileNo: mobileNo
					})
				})
				.then(res => {
					return res.json()
				})
				.then(data => {
					if (data === true){
						Router.push('/login')
					} else {
						Router.push('/errors/1')
					}
				})
			} else {
				Router.push('/errors/2')
			}
		})
	}
	// form => registerUser()

	useEffect(() => {
		
		if((email !== '' && password1 !== '' && password2 !== '') && (password1 === password2) && (mobileNo.length === 11)){
			setIsActive(true);
		}else{
			setIsActive(false);
		}
	},[email, password1, password2, mobileNo]);

	return (
		<Container>
            <Row className="justify-content-center mt-5">
                <Col>
					<Row>
			            <Col>
			              <Card className="mt-5 shadow-lg">
			        		<Card.Body className="m-5">
								<Form onSubmit={e => registerUser(e)}>
									<Form.Group>
										<Form.Label></Form.Label>
										<Form.Control 
											type="text"
											placeholder="First name"
											value={firstName}
											onChange={e => setFirstName(e.target.value)}
											required
										/>
									</Form.Group>

									<Form.Group>
										<Form.Control 
											type="text"
											placeholder="Last name"
											value={lastName}
											onChange={e => setLastName(e.target.value)}
											required
										/>
									</Form.Group>

									<Form.Group>
										<Form.Control 
											type="email"
											placeholder="Email"
											value={email}
											onChange={e => setEmail(e.target.value)}
											required
										/>
										<Form.Text className="text-muted">
											We'll never share your email with anyone else.
										</Form.Text>
									</Form.Group>

									<Form.Group>
										<Form.Control 
											type="password"
											placeholder="Password"
											value={password1}
											onChange={e => setPassword1(e.target.value)}
											required
										/>
									</Form.Group>

									<Form.Group>
										<Form.Control 
											type="password"
											placeholder="Confirm Password"
											value={password2}
											onChange={e => setPassword2(e.target.value)}
											required
										/>
									</Form.Group>

									<Form.Group>
										<Form.Control 
											type="number"
											placeholder="Input 11 digit phone number"
											value={mobileNo}
											onChange={e => setMobileNo(e.target.value)}
											required
										/>
									</Form.Group>

									{isActive
										?
										<Button 
											variant="outline-dark"
											type="submit"
											id="submitBtn"
										>
											SUBMIT
										</Button>
										:
										<Button 
											variant="outline-danger"
											type="submit"
											id="submitBtn"
											disabled
										>
											SUBMIT
										</Button>
									}
									</Form>
								</Card.Body>
							</Card>
						</Col>
			        </Row>
				</Col>
            </Row>
        </Container>	)
};