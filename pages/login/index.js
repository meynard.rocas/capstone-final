import { useState, useContext } from 'react';
import { Form, Button, Row, Col, Card, Container } from 'react-bootstrap';
import { GoogleLogin } from 'react-google-login';
import Swal from 'sweetalert2';

import Router from 'next/router';
import Head from 'next/head';

import UserContext from '../../UserContext';
import View from '../../components/View';
import AppHelper from '../../app-helper';

export default function index() {
    return (
        <Container>
            <Row className="justify-content-center m-5">
                <Col>
                    <LoginForm/>
                </Col>
            </Row>
        </Container>
    )
}

const LoginForm = () => {

    const { setUser } = useContext(UserContext);

	const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');

    const [tokenId, setTokenId] = useState(null);

    const [isActive, setIsActive] = useState(false);

    function authenticate(e) {

        //prevent redirection via form submission
        e.preventDefault();

        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/login`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if(data.error === 'does-not-exist') {
                    Swal.fire(
                        'Authentication Failed',
                        'User does not exist.',
                        'error'
                    )
                } else if (data.error === 'incorrect-password') {
                    Swal.fire(
                        'Authentication Failed',
                        'Password is incorrect.',
                        'error'
                    )
                } else if (data.error === 'login-type-error') {
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }
            }
        })

    }

    const authenticateGoogleToken = (response) => {
        console.log(response)

        const payload = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({ tokenId: response.tokenId })
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/verify-google-id-token`, payload)
        .then(AppHelper.toJSON)
        .then(data => {
            if (typeof data.accessToken !== 'undefined') {
                localStorage.setItem('token', data.accessToken)
                retrieveUserDetails(data.accessToken)
            } else {
                if (data.error === 'google-auth-error'){
                    Swal.fire(
                        'Google Auth Error',
                        'Google authentication procedure failed',
                        'error'
                    )
                } else if (data.error === 'login-type-error'){
                    Swal.fire(
                        'Login Type Error',
                        'You may have registered through a different login procedure',
                        'error'
                    )
                }
            }
        })
    }

    const retrieveUserDetails = (accessToken) => {
        const options = {
            headers: { 
                Authorization: `Bearer ${ accessToken }`
            }
        }

        fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/details`, options)
        .then(AppHelper.toJSON)
        .then(data => {
            setUser({
                id: data._id,
                isAdmin: data.isAdmin
            })

            Router.push('/transactions')
        })
    }

    // ternary operator = {condition ? ifTrue : ifFalse}
    return (
        <React.Fragment>
          <Row>
            <Col>
              <Card className="mt-5 shadow-lg">
                  <Card.Body className="m-5">
                      <Form onSubmit={e => authenticate(e)}>
                          <Form.Group controlId="userEmail">
                              <Form.Control 
                                  type="email" 
                                  placeholder="Email" 
                                  value={email}
                                  onChange={(e) => setEmail(e.target.value)}
                                  required
                              />
                          </Form.Group>

                          <Form.Group controlId="password">
                              <Form.Control 
                                  type="password" 
                                  placeholder="Password" 
                                  value={password}
                                  onChange={(e) => setPassword(e.target.value)}
                                  required
                              />
                          </Form.Group>

                          <Form.Group>
                              <Button className="text-center font-weight-bold w-100" variant="info" type="submit">
                                  SIGN IN
                              </Button>
                          </Form.Group>

                          <GoogleLogin
                              clientId="165640893560-9ofrch2c8sucbdpnf6ghr3m6rqfamd8b.apps.googleusercontent.com"
                              buttonText="SIGN IN WITH GOOGLE"
                              onSuccess={ authenticateGoogleToken }
                              onFailure={ authenticateGoogleToken }
                              cookiePolicy={ 'single_host_origin' }
                              className="w-100 text-center d-flex justify-content-center"
                          />
                      </Form>
                  </Card.Body>
              </Card>
            </Col>
          </Row>
        </React.Fragment>
    )
}